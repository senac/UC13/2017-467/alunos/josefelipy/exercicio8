
package br.com.senac.ex8.test;

import org.junit.Test;
import static org.junit.Assert.*;
import br.com.senac.ex8.Vendas;

public class VendasTest {
    
    public VendasTest() {
    }
    
    @Test
    public void ICMSemCimaDoRJ(){
        Vendas vendas = new Vendas("Saraiva RJ", "RJ" , 200);
        assertEquals(34, vendas.icmsRJ() , 0.1);
        
    }
    public void ICMSemCimaDoSP(){
        Vendas vendas = new Vendas("Saraiva SP", "SP" , 200);
        assertEquals(36, vendas.icmsSP() , 0.1);
        
    }
    public void ICMSemCimaDoMA(){
        Vendas vendas = new Vendas("Saraiva MA", "MA" , 200);
        assertEquals(24, vendas.icmsMA() , 0.1);
        
    }
}

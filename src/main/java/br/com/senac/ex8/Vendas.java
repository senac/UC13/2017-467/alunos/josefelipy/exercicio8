
package br.com.senac.ex8;

public class Vendas {
    
    private String cliente;
    private String estado;
    private double preco;
    

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public Vendas(String cliente, String estado, double preco) {
        this.cliente = cliente;
        this.estado = estado;
        this.preco = preco;
    }

    public Vendas() {
    }
    
    public double icmsRJ(){
        double imposto = (preco * 0.17);
        
        
        return imposto;
    }
     public double icmsSP(){
        double imposto = (preco * 0.18);
        
        
        return imposto;
    }
     public double icmsMA(){
        double imposto = (preco * 0.12);
        
        
        return imposto;
    }
    
}
